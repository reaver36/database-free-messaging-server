const _  = require('lodash');
const moment = require('moment-timezone');

const server = require('http').createServer();

class ContactCode{
    constructor(identifier, expiry){
        this.identifier = identifier;
        this.expiry = expiry;
    }
}

let clientList = {};//Should use DeviceInfo.getUniqueID() from react-native-device-info as the identifier...or a uuid
let contactCode = {};

// Clean expired contact codes out
setInterval(function(){
        console.log("cleaning contact codes");
        _.forEach(contactCode, function(value, key) {
            if(value.expiry < moment().valueOf()){
                console.log("cleaning contact codes" + key);
                delete contactCode[key];
            }
        });
    },
    60 * 1000//Every minute
);

const io = require('socket.io')(3000, {
    path: '/',
    serveClient: false,
    // below are engine.IO options
    pingInterval: 10000,
    pingTimeout: 5000,
    cookie: false
});

io.on('connection', function(socket){
    console.log('a user connected');
    // This should be emitted in the client inside the client.on('connection') handler
    socket.on('connectionComplete', (identifier, callback)=>{
        if(!identifier){
            socket.emit("appError", {message: "No identifier supplied"});
            if(callback)callback(0);
            return;
        }
        clientList[identifier] = socket.id;
        if(callback)callback(1);
    });

    socket.on('sendMessage', (data, callback)=>{
        if(!data.to){
            socket.emit("appError", {message: "No reciever supplied"});
            if(callback)callback(0);
            return;
        }
        if(!isOnline(data.to)){
            socket.emit("appError", {message: "Not online"});
            if(callback)callback(0);
            return;
        }
        socket.to([clientList[data.to]]).emit("message",
            {msg: data.msg, from: _.findKey(clientList, (val)=>val===socket.id)}
        );
        
        if(callback)callback(1);
    });

    socket.on('getContactCode', (identifier, callback)=>{
        if(!callback){
            return;
        }
        if(!clientList[identifier]){
            socket.emit("appError", {message: "Connection not found"});
            return;
        }
        let code = getUniqueCode();
        contactCode[code] = new ContactCode(identifier, moment().add(10, "minutes").tz("Etc/GMT").valueOf());
        callback({code, expiry: contactCode[code].expiry});
    });

    socket.on('getIdentifier', (code, callback)=>{
        if(!callback){
            return;
        }
        if(!contactCode[code]){
            socket.emit("appError", {message: "Contact not found"});
            return;
        }
        callback(contactCode[code].identifier);
    });

    socket.on('isOnline', (identifier, callback)=>{
        if(callback)callback(isOnline(identifier));
    });

    socket.on('disconnect', ()=>{
        let key = _.findKey(clientList, socket.id);
        if(clientList[key]){
            delete clientList[key];
        }
        console.log('user disconnected');
    });
});

function getUniqueCode(){
    let unique = false;
    let getRandomCode = function(){
        return Math.floor(Math.random() * Math.floor(1000000)).toString().padStart(6, '0');
    };

    let code = getRandomCode();
    while(!unique){
        if(!contactCode[code]){
            unique = true;
            continue;
        }
        code = getRandomCode();
    }
    return code;
}

function isOnline(identifier){
    return clientList[identifier] && io.sockets.connected[clientList[identifier]] != undefined && io.sockets.connected[clientList[identifier]].connected;
}
console.log("Server Started");