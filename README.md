## Events
### connect
### disconnect
### connectionComplete
### sendMessage
### getContactCode
### isOnline

## Environment Variables

## Deployment

## Known Problems
* it is possible to interrogate the db on a device and get a collection of contact identifiers then impersonate them. Possible Solution would be to identify contacts through a shared code unique to the connection between two clients rather than the specific identifier for a client.
* using UNIX timestamp for contact code expiry. This could cause display problems in the client if the date is set incorrectly. Should probably return the amount of time till expired instead.
* need moar tests
TODO: think of other problems
