const expect = require('chai').expect;
const SocketClient = require('socket.io-client');
const Server = require('../index');
const moment = require('moment-timezone');

describe("test server", function(){
    describe("test client can connect", function(){
        it("should connect without error", function(done){
            let socketClient = SocketClient('http://localhost:3000');
            socketClient.on('connect', function(){
                try{
                    expect(true).to.be.true;
                    done();
                }
                catch(err){
                    done(err);
                }
            });
            socketClient.on('connect_error', function(err){
                done(err);
            });
            socketClient.on('error', function(err){
                done(err);
            });
        });
    });

    describe("test client can send connection complete", function(){

        it("should throw an error when no identifier is supplied", function(done){
            let socketClient = SocketClient('http://localhost:3000');
            socketClient.on('connect', function(){
                socketClient.emit("connectionComplete", null, function(response){
                    try{
                        expect(response).to.equal(0);
                    }
                    catch(err){
                        done(err);
                    }
                });
            });
            socketClient.on('appError', function(err){
                try{
                    expect(err).to.have.property("message");
                    expect(err.message).to.equal("No identifier supplied");
                    done();
                }
                catch(err){
                    done(err);
                }
            });
            socketClient.on('connect_error', function(err){
                done(err);
            });
            socketClient.on('error', function(err){
                done(err);
            });
        });

        it("should store the socket when an identifier is supplied", function(done){
            let socketClient = SocketClient('http://localhost:3000');
            socketClient.on('connect', function(){
                socketClient.emit("connectionComplete", "testIdentifier", function(response){
                    try{
                        expect(response).to.be.equal(1);
                        socketClient.emit("isOnline", "testIdentifier", function(response){
                            expect(response).to.be.equal(true);
                            done();
                        });
                    }
                    catch(err){
                        done(err);
                    }
                });
            });
            socketClient.on('appError', function(err){
                done(err);
            });
            socketClient.on('connect_error', function(err){
                console.log("connecterror");
                done(err);
            });
            socketClient.on('error', function(err){
                done(err);
            });
        });
    });

    describe("send message to another person", function(){
        it("should fail if reciever is not supplied", function(done){
            let socketClient = SocketClient('http://localhost:3000');
            socketClient.on('connect', function(){
                socketClient.emit("connectionComplete", {identifier: "testIdentifier"}, function(response){
                    try{
                        expect(response).to.be.equal(1);
                        socketClient.emit("sendMessage", {msg: "test message"}, function(response){
                            try{
                                expect(response).to.equal(0);
                            }
                            catch(err){
                                done(err);
                            }
                        });
                    }
                    catch(err){
                        done(err);
                    }
                });
            });
            socketClient.on('appError', function(err){
                try{
                    expect(err).to.have.property("message");
                    expect(err.message).to.equal("No reciever supplied");
                    done();
                }
                catch(err){
                    done(err);
                }
            });
            socketClient.on('connect_error', function(err){
                done(err);
            });
            socketClient.on('error', function(err){
                done(err);
            });
        });

        it("should fail if person is not online", function(done){
            let socketClient = SocketClient('http://localhost:3000');
            socketClient.on('connect', function(){
                socketClient.emit("connectionComplete", "testIdentifier", function(response){
                    try{
                        expect(response).to.be.equal(1);
                        socketClient.emit("sendMessage", {msg: "test message", to: "someGuy"}, function(response){
                            try{
                                expect(response).to.equal(0);
                            }
                            catch(err){
                                done(err);
                            }
                        });
                    }
                    catch(err){
                        done(err);
                    }
                });
            });
            socketClient.on('appError', function(err){
                try{
                    expect(err).to.have.property("message");
                    expect(err.message).to.equal("Not online");
                    done();
                }
                catch(err){
                    done(err);
                }
            });
            socketClient.on('connect_error', function(err){
                done(err);
            });
            socketClient.on('error', function(err){
                done(err);
            });
        });

        it("should send person message", function(done){
            let socketClient = SocketClient('http://localhost:3000');
            let socketClient2 = SocketClient('http://localhost:3000');
            socketClient.on('connect', function(){
                socketClient.emit("connectionComplete", "testIdentifier", function(response){
                    try{
                        expect(response).to.be.equal(1);
                        socketClient.emit("sendMessage", {msg: "test message", to: "testIdentifier2"}, function(response){
                            try{
                                expect(response).to.equal(1);
                            }
                            catch(err){
                                done(err);
                            }
                        });
                    }
                    catch(err){
                        done(err);
                    }
                });
            });
            socketClient.on('appError', function(err){
                done(err);
            });
            socketClient.on('connect_error', function(err){
                done(err);
            });
            socketClient.on('error', function(err){
                done(err);
            });

            socketClient2.on('connect', function(){
                socketClient2.emit("connectionComplete", "testIdentifier2", function(response){
                    try{
                        expect(response).to.be.equal(1);
                    }
                    catch(err){
                        done(err);
                    }
                });
            });
            socketClient2.on('message', function(message){
                try{
                    expect(message).to.have.property("from");
                    expect(message).to.have.property("msg");
                    expect(message.from).to.equal("testIdentifier");
                    expect(message.msg).to.equal("test message");
                    done();
                }
                catch(err){
                    done(err);
                }
            });
            socketClient2.on('appError', function(err){
                done(err);
            });
            socketClient2.on('connect_error', function(err){
                done(err);
            });
            socketClient2.on('error', function(err){
                done(err);
            });
        });
    });

    describe("get contact code", function(){
        it("should get a new contact code and expiry when requested", function(done){
            let socketClient = SocketClient('http://localhost:3000');
            socketClient.on('connect', function(){
                socketClient.emit("connectionComplete", "testIdentifier", function(response){
                    try{
                        expect(response).to.be.equal(1);
                        socketClient.emit("getContactCode", "testIdentifier", function(response){
                            try{
                                expect(response).to.have.property("code");
                                expect(response).to.have.property("expiry");
                                expect(response.code).to.have.lengthOf(6);
                                expect(response.code).to.match(/^\d{6}$/);
                                expect(response.expiry).to.be.above(moment().valueOf());
                                done();
                            }
                            catch(err){
                                done(err);
                            }
                        });
                    }
                    catch(err){
                        done(err);
                    }
                });
            });
            socketClient.on('appError', function(err){
                done(err);
            });
            socketClient.on('connect_error', function(err){
                done(err);
            });
            socketClient.on('error', function(err){
                done(err);
            });
        });
    });

    describe("get identifier by contact code", function(){
        it("should get an identifier based on a contact code when requested", function(done){
            let socketClient = SocketClient('http://localhost:3000');
            socketClient.on('connect', function(){
                socketClient.emit("connectionComplete", "testIdentifier", function(response){
                    try{
                        expect(response).to.be.equal(1);
                        socketClient.emit("getContactCode", "testIdentifier", function(response){
                            try{
                                expect(response).to.have.property("code");
                                expect(response).to.have.property("expiry");
                                expect(response.code).to.have.lengthOf(6);
                                expect(response.code).to.match(/^\d{6}$/);
                                expect(response.expiry).to.be.above(moment().valueOf());
                                socketClient.emit("getIdentifier", response.code, function(response){
                                    try{
                                        expect(response).to.equal("testIdentifier");
                                        done();
                                    }
                                    catch(err){
                                        done(err);
                                    }
                                });
                            }
                            catch(err){
                                done(err);
                            }
                        });
                    }
                    catch(err){
                        done(err);
                    }
                });
            });
            socketClient.on('appError', function(err){
                done(err);
            });
            socketClient.on('connect_error', function(err){
                done(err);
            });
            socketClient.on('error', function(err){
                done(err);
            });
        });
    });
});